use std::fs;

fn execute_program(mut a: Amplifier) -> Amplifier{
    while a.tokens[a.current_op] != 99{
        match a.tokens[a.current_op] % 100{
            1 => { //Add
                let tempop = a.tokens[a.current_op + 3] as usize;
                let first = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                a.tokens[tempop] = first + second;
                a.current_op += 4;
            }
            2 => { //Multiply
                let tempop = a.tokens[a.current_op + 3] as usize;
                let first = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                a.tokens[tempop] = first * second;
                a.current_op += 4;
            }
            3 => { //Input
                if a.input.len() == 0{
                    return a;
                }
                let number = a.input.remove(0);
                let tempop = a.tokens[a.current_op + 1] as usize;
                a.tokens[tempop] = number;
                a.current_op += 2;
            }
            4 => { //Output
                let tempop = a.tokens[a.current_op + 1];
                let first_param = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[tempop as usize]} else {tempop};
                // println!("{}", first_param);
                a.output.push(first_param);
                a.current_op += 2;
            }
            5 => { //Jump if true
                let first_param = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second_param = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                if first_param == 0{a.current_op += 3}
                else{a.current_op = second_param as usize;}
            }
            6 => { //Jump if false
                let first_param = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second_param = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                if first_param != 0{a.current_op += 3}
                else{a.current_op = second_param as usize;}
            }
            7 => { //Less than
                let first_param = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second_param = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                let third_param = a.tokens[a.current_op + 3];
                a.tokens[third_param as usize] = if first_param < second_param {1} else {0};
                a.current_op += 4;
            }
            8 => { //Equals
                let first_param = if (a.tokens[a.current_op] % 1000) / 100 == 0 {a.tokens[a.tokens[a.current_op + 1] as usize]} else {a.tokens[a.current_op + 1]};
                let second_param = if (a.tokens[a.current_op] % 10000) / 1000 == 0 {a.tokens[a.tokens[a.current_op + 2] as usize]} else {a.tokens[a.current_op + 2]};
                let third_param = a.tokens[a.current_op + 3];
                a.tokens[third_param as usize] = if first_param == second_param {1} else {0};
                a.current_op += 4;
            }
             _ => {
                println!("Unexpected command!");
                std::process::exit(1);
            }
        }
    }
    a.finished = true;
    a
}

fn read_file() -> Vec<i32>{
    let filename = "src/day7/input";
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    contents.lines().next().unwrap().split(",").map(|x| x.parse::<i32>().unwrap()).collect()
}

pub fn day7a() {
    let mut highest_final_output = 0;
    let mut permutations: Vec<[i32; 5]> = Vec::new();
    for a in 0..5{
        for b in 0..5{
            if a == b{continue;}
            for c in 0..5{
                if a == c || b == c{continue;}
                for d in 0..5{
                    if a == d || b == d || c == d{continue;}
                    for e in 0..5{
                        if a == e || b == e || c == e || d == e{continue;}
                        permutations.push([a, b, c, d, e]);
                    }
                }
            }

        }
    }
    for x in permutations{
        let mut output: Vec<i32> = Vec::new();
        output.push(0);
        for y in x.iter(){
            let mut input: Vec<i32> = Vec::new();
            input.push(*y);
            input.push(output[0]);
            let mut amp = Amplifier{
                current_op: 0,
                finished: false,
                input: input,
                tokens: read_file(),
                output: Vec::new()
            };
            amp = execute_program(amp);
            output = amp.output;
        }
        if highest_final_output < output[0] {
            highest_final_output = output[0];
            println!("{:?}", output);
            println!("{:?}", x);
        }
    }
    println!("{:?}", highest_final_output);
}

#[derive(Debug)]
struct Amplifier{
    current_op: usize,
    tokens: Vec<i32>,
    finished: bool,
    input: Vec<i32>,
    output: Vec<i32>
}

impl Clone for Amplifier {
    fn clone(&self) -> Amplifier {
        Amplifier{
            current_op: self.current_op,
            finished: self.finished,
            input: self.input.clone(), 
            output: self.output.clone(), 
            tokens: self.tokens.clone()
        }
    }
}

pub fn day7b() {
    let mut highest_final_output = 0;
    let mut permutations: Vec<[i32; 5]> = Vec::new();
    for a in 5..10{
        for b in 5..10{
            if a == b{continue;}
            for c in 5..10{
                if a == c || b == c{continue;}
                for d in 5..10{
                    if a == d || b == d || c == d{continue;}
                    for e in 5..10{
                        if a == e || b == e || c == e || d == e{continue;}
                        permutations.push([a, b, c, d, e]);
                    }
                }
            }

        }
    }
    for x in permutations{
        let mut amplifiers: Vec<Amplifier> = Vec::new();
        for y in x.iter(){
            let mut input: Vec<i32> = Vec::new();
            input.push(*y);
            let amp = Amplifier{
                current_op: 0,
                finished: false,
                input: input,
                tokens: read_file(),
                output: Vec::new()
            };
            amplifiers.push(amp)
        }
        amplifiers[0].input.push(0);
        while !amplifiers[4].finished{
            for i in 0..5{
                let mut output_clone = amplifiers[(i+4)%5].output.clone();
                amplifiers[i].input.append(&mut output_clone);
                amplifiers[(i+4)%5].output.clear();
                amplifiers[i] = execute_program(amplifiers[i].clone());
            }
        }
        if highest_final_output < amplifiers[4].output[0] {
            highest_final_output = amplifiers[4].output[0];
            println!("{:?}", amplifiers[4].output);
            println!("{:?}", x);
        }
    }
    println!("{:?}", highest_final_output);
}