use std::fs;
use std::io;

pub fn day5() {
    let filename = "src/day5/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
        
    for line in contents.lines(){
        let mut tokens:Vec<i32>= line.split(",").map(|x| x.parse::<i32>().unwrap()).collect();
        let mut nextop = 0;
        while tokens[nextop] != 99{
            match tokens[nextop] % 100{
                1 => { //Add
                    let tempop = tokens[nextop + 3] as usize;
                    let first = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    tokens[tempop] = first + second;
                    nextop += 4;
                }
                2 => { //Multiply
                    let tempop = tokens[nextop + 3] as usize;
                    let first = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    tokens[tempop] = first * second;
                    nextop += 4;
                }
                3 => { //Input
                    println!("Enter number");
                    let mut input_text = String::new();
                    io::stdin().read_line(&mut input_text).expect("failed to read from stdin");
                    let number = input_text.trim().parse::<i32>().unwrap();
                    let tempop = tokens[nextop + 1] as usize;
                    tokens[tempop] = number;
                    nextop += 2;
                }
                4 => { //Output
                    let tempop = tokens[nextop + 1];
                    let first_param = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tempop as usize]} else {tempop};
                    println!("{}", first_param);
                    nextop += 2;
                }
                5 => { //Jump if true
                    let first_param = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second_param = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    if first_param == 0{nextop += 3}
                    else{nextop = second_param as usize;}
                }
                6 => { //Jump if false
                    let first_param = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second_param = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    if first_param != 0{nextop += 3}
                    else{nextop = second_param as usize;}
                }
                7 => { //Less than
                    let first_param = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second_param = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    let third_param = tokens[nextop + 3];
                    tokens[third_param as usize] = if first_param < second_param {1} else {0};
                    nextop += 4;
                }
                8 => { //Equals
                    let first_param = if (tokens[nextop] % 1000) / 100 == 0 {tokens[tokens[nextop + 1] as usize]} else {tokens[nextop + 1]};
                    let second_param = if (tokens[nextop] % 10000) / 1000 == 0 {tokens[tokens[nextop + 2] as usize]} else {tokens[nextop + 2]};
                    let third_param = tokens[nextop + 3];
                    tokens[third_param as usize] = if first_param == second_param {1} else {0};
                    nextop += 4;
                }
                 _ => {
                    println!("Unexpected command!");
                    std::process::exit(1);
                }
            }
        }
        println!("Finished execution!");
    }
}