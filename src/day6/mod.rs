use std::fs;
use std::collections::HashMap;

pub fn day6a() {
    let filename = "src/day6/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut dict: HashMap<String, Vec<String>> = HashMap::new();
    for line in contents.lines(){
        println!("{:?}", line);
        let mut iter = line.split(")");
        let orbitted_object = iter.next().unwrap();
        let orbitting_object = iter.next().unwrap();
        if dict.contains_key(orbitted_object){
            let vec = dict.get_mut(orbitted_object).unwrap();
            vec.push(orbitting_object.to_string());
        }
        else{
            let mut vec = Vec::<String>::new();
            vec.push(orbitting_object.to_string());
            dict.insert(orbitted_object.to_string(), vec);
        }
        if !dict.contains_key(orbitting_object){
            dict.insert(orbitting_object.to_string(), Vec::<String>::new());
        }
    }
    let mut orbits: HashMap<String, u32> = HashMap::new();
    let mut last = false;
    while !last{
        let ends:Vec<String> = IntoIterator::into_iter(dict.to_owned()).filter_map(|(key, vec)| {
            if vec.len() == 0{Some(key.to_owned())}
            else {None}
        }).collect();
        last = ends.len() == 0;
        for end in ends{
            dict.remove(&end);
            for key in dict.keys(){
                if dict[key].contains(&end){
                    *orbits.entry(key.to_string()).or_insert(0) += if orbits.contains_key(&end) {orbits[&end] + 1} else {1};
                }
            }
            for vec in dict.values_mut(){
                vec.retain(|x| *x != end);
            }
        }
    }
    println!("{:?}", orbits);
    let total: u32 = orbits.values().sum();
    println!("{}", total);
}

pub fn day6b() {
    let filename = "src/day6/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut dict: HashMap<String, Vec<String>> = HashMap::new();
    for line in contents.lines(){
        println!("{:?}", line);
        let mut iter = line.split(")");
        let orbitted_object = iter.next().unwrap();
        let orbitting_object = iter.next().unwrap();
        if dict.contains_key(orbitted_object){
            let vec = dict.get_mut(orbitted_object).unwrap();
            vec.push(orbitting_object.to_string());
        }
        else{
            let mut vec = Vec::<String>::new();
            vec.push(orbitting_object.to_string());
            dict.insert(orbitted_object.to_string(), vec);
        }
        if !dict.contains_key(orbitting_object){
            dict.insert(orbitting_object.to_string(), Vec::<String>::new());
        }
    }
    let mut santa_path = Vec::<String>::new();
    let mut my_path = Vec::<String>::new();
    let mut path_found = false;
    let mut current_santa_node: String = "SAN".to_string();
    let mut current_my_node: String = "YOU".to_string();
    while !path_found{
        for (key, vec) in dict.to_owned(){
            if vec.iter().any(|x| x == &current_santa_node){
                current_santa_node = key.to_string();
                santa_path.push(key);
            }
        }
        for (key, vec) in dict.to_owned(){
            if vec.iter().any(|x| x == &current_my_node){
                current_my_node = key.to_string();
                my_path.push(key);
            }
        }
        path_found = current_my_node == current_santa_node;
    }
    let mut nodes_in_common: Vec<String> = Vec::new();
    for x in &mut santa_path{
        if my_path.contains(x){nodes_in_common.push(x.to_string())}
    }
    santa_path.retain(|x| !nodes_in_common.contains(x));
    my_path.retain(|x| !nodes_in_common.contains(x));

    println!("{:?}", santa_path.len() + my_path.len());
}