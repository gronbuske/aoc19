use std::fs;

pub fn day8() {
    let filename = "src/day8/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let width = 25;
    let height = 6;
    let mut iter = contents.chars();
    let mut fewest_zeroes = width*height;
    let mut answer = 0;
    let mut first_picture: Vec<u32> = Vec::new();
    while let Some(c) = iter.next(){
        let mut this_picture: Vec<u32> = Vec::new();
        let mut ch = c;
        if first_picture.len() == 0{
            first_picture.push(ch.to_digit(10).unwrap());
            for _ in 0..width*height-1{
                ch = iter.next().unwrap();
                first_picture.push(ch.to_digit(10).unwrap());
            }
            this_picture = first_picture.clone();
        }
        else{
            this_picture.push(ch.to_digit(10).unwrap());
            for _ in 0..width*height-1{
                ch = iter.next().unwrap();
                this_picture.push(ch.to_digit(10).unwrap());
            }
        }
        println!("");
        print!("F: ");
        for a in first_picture.to_owned(){
            print!("{}", a);
        }
        println!("");
        print!("T: ");
        for a in this_picture.to_owned(){
            print!("{}", a);
        }
        println!("");
        let zeroes = this_picture.iter().filter(|&x| *x == 0).count();
        if zeroes < fewest_zeroes{
            fewest_zeroes = zeroes;
            let ones = this_picture.iter().filter(|&x| *x == 1).count() as u32;
            let twos = this_picture.iter().filter(|&x| *x == 2).count() as u32;
            answer = ones*twos;
        }

        for i in 0..first_picture.len(){
            if first_picture[i] == 2 {
                first_picture[i] = this_picture[i];
            }
        }
    }
    println!("{:?}", answer);
    println!("");

    let mut iter = first_picture.iter();
    for w in 0..height{
        for h in 0..width{
            print!("{}", if iter.next().unwrap() == &1 {"X"} else {" "});
        }
        println!();
    }
}
