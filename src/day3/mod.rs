use std::fs;
use std::cmp;

pub fn day3() {
    let filename = "src/day3/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    //let mut crossings = Vec::new();

    let first_wire:Vec<(char, i32)> = contents.lines().nth(6).unwrap().split(",").map(|x| (x.chars().next().unwrap(), x[1..x.len()].parse::<i32>().unwrap())).collect();
    let second_wire:Vec<(char, i32)> = contents.lines().nth(7).unwrap().split(",").map(|x| (x.chars().next().unwrap(), x[1..x.len()].parse::<i32>().unwrap())).collect();

    let mut first_path:Vec<(i64, i64)> = Vec::new();
    let mut intersections:Vec<(i64, i64)> = Vec::new();
    let mut position = (0, 0);
    for (direction, length) in first_wire{
        match direction{
            // Right
            'R' => {for _ in 0..length {position.0 += 1; first_path.push(position);}},
            // Left
            'L' => {for _ in 0..length {position.0 -= 1; first_path.push(position);}},
            // Up
            'U' => {for _ in 0..length {position.1 += 1; first_path.push(position);}},
            // Down
            'D' => {for _ in 0..length {position.1 -= 1; first_path.push(position);}},
            _ => println!("Unknown direction!")
        }
    }
    let mut shortest_manhattan = std::i64::MAX;
    let mut shortest_distance = std::i64::MAX;
    let mut distance = 0;
    position = (0, 0);
    for (direction, length) in second_wire{
        match direction{
            // Right
            'R' => {
                for _ in 0..length {
                    //part 1
                    position.0 += 1; 
                    if position.0.abs() + position.1.abs() < shortest_manhattan && first_path.contains(&position) {
                        println!("{:?}", position); intersections.push(position);
                        shortest_manhattan = position.0.abs() + position.1.abs();
                    }
                    //part 2
                    distance += 1;
                    for step in 0..cmp::min(shortest_distance-distance, first_path.len() as i64){
                        if position == first_path[step as usize]{
                            shortest_distance = distance + step + 1;
                            println!("{}", shortest_distance);
                        }
                    }
                }
            },
            // Left
            'L' => {
                for _ in 0..length {
                    //part 1
                    position.0 -= 1; 
                    if position.0.abs() + position.1.abs() < shortest_manhattan && first_path.contains(&position) {
                        println!("{:?}", position); intersections.push(position);
                        shortest_manhattan = position.0.abs() + position.1.abs();
                    }
                    //part 2
                    distance += 1;
                    for step in 0..cmp::min(shortest_distance-distance, first_path.len() as i64){
                        if position == first_path[step as usize]{
                            shortest_distance = distance + step + 1;
                            println!("{}", shortest_distance);
                        }
                    }
                }
            },
            // Up
            'U' => {
                for _ in 0..length {
                    //part 1
                    position.1 += 1; 
                    if position.0.abs() + position.1.abs() < shortest_manhattan && first_path.contains(&position) {
                        println!("{:?}", position); intersections.push(position);
                        shortest_manhattan = position.0.abs() + position.1.abs();
                    }
                    //part 2
                    distance += 1;
                    for step in 0..cmp::min(shortest_distance-distance, first_path.len() as i64){
                        if position == first_path[step as usize]{
                            shortest_distance = distance + step + 1;
                            println!("{}", shortest_distance);
                        }
                    }
                }
            },
            // Down
            'D' => {
                for _ in 0..length {
                    //part 1
                    position.1 -= 1; 
                    if position.0.abs() + position.1.abs() < shortest_manhattan && first_path.contains(&position) {
                        println!("{:?}", position); intersections.push(position);
                        shortest_manhattan = position.0.abs() + position.1.abs();
                    }
                    //part 2
                    distance += 1;
                    for step in 0..cmp::min(shortest_distance-distance, first_path.len() as i64){
                        if position == first_path[step as usize]{
                            shortest_distance = distance + step + 1;
                            println!("{}", shortest_distance);
                        }
                    }
                }
            },
            _ => println!("Unknown direction!")
        }
    }
    
    println!("{}", shortest_manhattan);
    println!("{}", shortest_distance);
}