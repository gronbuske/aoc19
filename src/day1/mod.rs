use std::fs;

pub fn day1() {
    let filename = "src/day1/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut result = 0;
    let mut result_extra = 0;

    for line in contents.lines() {
        let weight = line.parse::<i32>().unwrap();
        let requirement = weight / 3 - 2;
        let mut extra = requirement;
        result_extra += requirement;

        while extra > 0 {
            result += extra;
            extra = extra / 3 - 2;
        }
    }

    println!("Fuel weight: {}", result_extra);
    println!("Fuel weight, with extra for fuel: {}", result);
}