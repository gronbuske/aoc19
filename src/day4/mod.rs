use std::fs;

pub fn day4() {
    let mut current: [i8; 6] = [1,3,6,7,7,7];
    let last: [i8; 6] = [5,8,9,9,9,9];
    let mut passwords = 0;
    let mut strict_passwords = 0;

    while current != last{
        let mut double_val = false;
        let mut exactly_double_val = false;
        for i in 0..5{
            if current[i] == current[i+1]{
                double_val = true;
                let mut empty_before = i == 0;
                if !empty_before {empty_before = current[i-1] != current[i];}
                let mut empty_after = i >= 4;
                if !empty_after {empty_after = current[i+2] != current[i];}
                if empty_before && empty_after {exactly_double_val = true;}
            }
        }
        println!("{:?}", current);
        if double_val {passwords += 1;}
        if exactly_double_val {
            strict_passwords += 1;}

        let mut has_nine = false;
        for i in 0..6 {
            if current[i] == 9 && !has_nine{
                has_nine = true;
                let new_high = current[i-1]+1;
                for j in i-1..6{
                    current[j] = new_high;
                }
            }
        }
        if !has_nine {current[5] += 1};
    }
    println!("{}", passwords + 1);
    println!("{}", strict_passwords);
}