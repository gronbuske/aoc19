use std::fs;

pub fn day2() {
    let filename = "src/day2/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    let goal = 19690720;

    for line in contents.lines(){
        let mut tokens:Vec<i32>= line.split(",").map(|x| x.parse::<i32>().unwrap()).collect();
        while tokens[0] != goal{
            let last0 = tokens[0];
            let last1 = tokens[1];
            let last2 = tokens[2];
            tokens = line.split(",").map(|x| x.parse::<i32>().unwrap()).collect();
            tokens[1] += if last0 < goal {last1 + 1} else {last1};
            tokens[2] += if last0 % 1000 == goal % 1000 {last2} else {last2 + 1};

            let mut nextop = 0;
            while tokens[nextop] != 99{
                if tokens[nextop] == 1{
                    let tempop = tokens[nextop + 3] as usize;
                    tokens[tempop] = tokens[tokens[nextop + 1] as usize] + tokens[tokens[nextop + 2] as usize];
                }
                else if tokens[nextop] == 2{
                    let tempop = tokens[nextop + 3] as usize;
                    tokens[tempop] = tokens[tokens[nextop + 1] as usize] * tokens[tokens[nextop + 2] as usize];
                }
                nextop += 4;
            }
            //println!("{}-{}-{}", tokens[0], tokens[1], tokens[2]);
        }
        println!("{}", tokens[1] * 100 + tokens[2]);
    }
}