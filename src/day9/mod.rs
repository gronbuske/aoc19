use std::fs;

fn execute_program(mut a: Program) -> Program{
    while a.instructions[a.current_op] != 99{
        match a.instructions[a.current_op] % 100{
            1 => { //Add
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 1]) as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 2]) as usize]};

                if (a.instructions[a.current_op] % 100000) / 10000 == 2{
                    let tempop = a.instructions[a.current_op + 3];
                    a.instructions[(a.relative_base + tempop) as usize] = first + second;
                } else{
                    let tempop = a.instructions[a.current_op + 3] as usize;
                    a.instructions[tempop] = first + second;
                }
                a.current_op += 4;
            }
            2 => { //Multiply
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[a.relative_base as usize + a.instructions[a.current_op + 1] as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 2]) as usize]};

                if (a.instructions[a.current_op] % 100000) / 10000 == 2{
                    let tempop = a.instructions[a.current_op + 3];
                    a.instructions[(a.relative_base + tempop) as usize] = first * second;
                } else{
                    let tempop = a.instructions[a.current_op + 3] as usize;
                    a.instructions[tempop] = first * second;
                }
                a.current_op += 4;
            }
            3 => { //Input
                if a.input.len() == 0{ return a; }

                let number = a.input.remove(0);
                let tempop = a.instructions[a.current_op + 1] as usize;
                if (a.instructions[a.current_op] % 1000) / 100 == 2{
                    a.instructions[a.relative_base as usize + tempop] = number;
                } else{
                    a.instructions[tempop] = number;
                }
                a.current_op += 2;
            }
            4 => { //Output
                let tempop = a.instructions[a.current_op + 1];
                let mode = (a.instructions[a.current_op] % 1000) / 100;
                let first_param = if mode == 0 {a.instructions[tempop as usize]} 
                else if mode == 1 {tempop}
                else {a.instructions[a.relative_base as usize + tempop as usize]};
                a.output.push(first_param);
                a.current_op += 2;
            }
            5 => { //Jump if true
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 1]) as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 2]) as usize]};

                if first == 0 {a.current_op += 3}
                else {a.current_op = second as usize;}
            }
            6 => { //Jump if false
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 1]) as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else { a.instructions[(a.relative_base + a.instructions[a.current_op + 2]) as usize]};
                if first != 0 {a.current_op += 3}
                else {a.current_op = second as usize;}
            }
            7 => { //Less than
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[(a.relative_base + a.instructions[a.current_op + 1]) as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else {a.instructions[a.relative_base as usize + a.instructions[a.current_op + 2] as usize]};

                if (a.instructions[a.current_op] % 100000) / 10000 == 2{
                    let third = a.instructions[a.current_op + 3];
                    a.instructions[(a.relative_base + third) as usize] = if first < second {1} 
                    else {0};
                } else{
                    let third = a.instructions[a.current_op + 3];
                    a.instructions[third as usize] = if first < second {1}
                    else {0};
                }

                a.current_op += 4;
            }
            8 => { //Equals
                let firstmode = (a.instructions[a.current_op] % 1000) / 100;
                let first = if firstmode == 0 {a.instructions[a.instructions[a.current_op + 1] as usize]} 
                else if firstmode == 1 {a.instructions[a.current_op + 1]}
                else {a.instructions[a.relative_base as usize + a.instructions[a.current_op + 1] as usize]};
                
                let secondmode = (a.instructions[a.current_op] % 10000) / 1000;
                let second = if secondmode == 0 {a.instructions[a.instructions[a.current_op + 2] as usize]} 
                else if secondmode == 1 {a.instructions[a.current_op + 2]}
                else {a.instructions[a.relative_base as usize + a.instructions[a.current_op + 2] as usize]};

                if (a.instructions[a.current_op] % 100000) / 10000 == 2{
                    let third = a.instructions[a.current_op + 3];
                    a.instructions[(a.relative_base + third) as usize] = if first == second {1} 
                    else {0};
                } else{
                    let third = a.instructions[a.current_op + 3];
                    a.instructions[third as usize] = if first == second {1} 
                    else {0};
                }

                a.current_op += 4;
            }
            9 => { //Change relative base
                let tempop = a.instructions[a.current_op + 1];
                let mode = (a.instructions[a.current_op] % 1000) / 100;
                let change = if mode == 0 {a.instructions[tempop as usize]} 
                else if mode == 1 {tempop}
                else {a.instructions[a.relative_base as usize + tempop as usize]};
                a.relative_base += change;
                a.current_op += 2;
            }
             _ => {
                println!("Unexpected command!");
                std::process::exit(1);
            }
        }
    }
    a.finished = true;
    a
}

fn read_file() -> Vec<i64>{
    let filename = "src/day9/input";
    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");
    contents.lines().next().unwrap().split(",").map(|x| x.parse::<i64>().unwrap()).collect()
}

#[derive(Debug)]
struct Program {
    current_op: usize,
    instructions: Vec<i64>,
    finished: bool,
    input: Vec<i64>,
    output: Vec<i64>,
    relative_base: i64
}

impl Clone for Program {
    fn clone(&self) -> Program {
        Program{
            current_op: self.current_op,
            finished: self.finished,
            input: self.input.clone(), 
            output: self.output.clone(), 
            instructions: self.instructions.clone(),
            relative_base: self.relative_base
        }
    }
}

pub fn day9() {
    let mut program = Program{
            current_op: 0,
            finished: false,
            input: Vec::new(),
            output: Vec::new(), 
            instructions: Vec::new(),
            relative_base: 0
    };
    program.input.push(2);
    program.instructions = read_file();
    for _ in 0..10000{
        program.instructions.push(0);
    }
    program = execute_program(program);
    println!("{:?}", program.output);
}