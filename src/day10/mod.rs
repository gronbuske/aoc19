use std::fs;
use std::cmp;

pub fn day10() {
    let filename = "src/day10/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut v: Vec<(i32, i32)> = Vec::new();
    let mut x = 0;
    let mut y = 0;
    for line in contents.lines(){
        x = 0;
        for c in line.chars(){
            if c == '#'{
                v.push((x, y));
            }
            x += 1;
        }
        y += 1;
    }

    let mut max_visible = 0;
    let mut best_position = (0, 0);
    for a in &v{
        let mut count_visible = 0;
        for b in &v{
            if a != b{
                let vec = (b.0 - a.0, b.1 - a.1);
                let mut visible = true;

                for c in &v{
                    if a != c && b != c{
                        let vec2 = (c.0 - a.0, c.1 - a.1);
                        if cmp::max(a.0, b.0) >= c.0 && cmp::max(a.1, b.1) >= c.1 && cmp::min(a.0, b.0) <= c.0 && cmp::min(a.0, b.0) <= c.0{
                            if vec.0 == 0 || vec.1 == 0 || vec2.0 == 0 || vec2.1 == 0{
                                if (vec.0 == 0 && vec2.0 == 0) || (vec.1 == 0 && vec2.1 == 0){
                                    visible = false;
                                    // println!("0a: a-{:?} c-{:?} b-{:?}", a, c, b); 
                                    //println!("vec: {:?} vec2: {:?}", vec, vec2);
                                }
                            } else if vec.0 as f32 / vec.1 as f32 == vec2.0 as f32 / vec2.1 as f32{
                                visible = false;
                                println!("0a: a-{:?} c-{:?} b-{:?}", a, c, b); 
                            }
                        }
                    }
                }
                if visible { 
                    println!("Visible: a-{:?} b-{:?}", a, b); 
                    count_visible += 1;
                }
            }
        }
        if count_visible > max_visible{
            best_position = *a;
            max_visible = count_visible;
        }
    }
    println!("{}", max_visible);


}
