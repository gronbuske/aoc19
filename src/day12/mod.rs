use std::fs;
use std::num;

#[derive(Debug, Copy, Clone)]
struct Moon {
    xp: i64,
    yp: i64,
    zp: i64,
    xv: i64,
    yv: i64,
    zv: i64
}

pub fn day12() {
    let filename = "src/day12/input";

    println!("In file {}", filename);

    let contents = fs::read_to_string(filename)
        .expect("Something went wrong reading the file");

    let mut m = [Moon{xp: 0, yp: 0, zp: 0, xv: 0, yv: 0, zv: 0}; 4];
    let mut counter = 0 as usize;
    for line in contents.lines(){
        let clean_line = line.replace("<", "").replace(">", "").replace(",", "");
        let mut parts = clean_line.trim().split(" ");
        m[counter].xp = parts.next().unwrap().split("=").nth(1).unwrap().parse::<i64>().unwrap();
        m[counter].yp = parts.next().unwrap().split("=").nth(1).unwrap().parse::<i64>().unwrap();
        m[counter].zp = parts.next().unwrap().split("=").nth(1).unwrap().parse::<i64>().unwrap();
        counter += 1;
    }

    let mut lastx = 0;
    let mut lasty = 0;
    let mut lastz = 0;

    for i in 0..1000000 as u64{
        // Apply gravity
        for a in 0..4{
            for b in 0..4{
                if a != b{
                    m[a].xv += if m[a].xp < m[b].xp {1} else if m[a].xp == m[b].xp {0} else {-1};
                    m[a].yv += if m[a].yp < m[b].yp {1} else if m[a].yp == m[b].yp {0} else {-1};
                    m[a].zv += if m[a].zp < m[b].zp {1} else if m[a].zp == m[b].zp {0} else {-1};
                }
            }
        }

        // Apply velocity
        for a in 0..4{
            m[a].xp += m[a].xv;
            m[a].yp += m[a].yv;
            m[a].zp += m[a].zv;
        }

        // if m[0].xp == -4 {println!("0.x {:?}", i)}
        // if m[0].yp == -9 {println!("0.y {:?}", i)}
        // if m[0].zp == -3 {println!("0.z {:?}", i)}
        // if m[1].xp == -13 {println!("1.x {:?}", i)}
        // if m[1].yp == -11 {println!("1.y {:?}", i)}
        // if m[1].zp == 0 {println!("1.z {:?}", i)}
        // if m[2].xp == -17 {println!("2.x {:?}", i)}
        // if m[2].yp == -7 {println!("2.y {:?}", i)}
        // if m[2].zp == 15 {println!("2.z {:?}", i)}
        // if m[3].xp == -16 {println!("3.x {:?}", i)}
        // if m[3].yp == 4 {println!("3.y {:?}", i)}
        // if m[3].zp == 2 {println!("3.z {:?}", i)}
        
        // if m[0].xv == 0 {println!("0.x {:?}", i)}
        // if m[0].yv == 0 {println!("0.y {:?}", i)}
        // if m[0].zv == 0 {println!("0.z {:?}", i)}
        // if m[1].xv == 0 {println!("1.x {:?}", i)}
        // if m[1].yv == 0 {println!("1.y {:?}", i)}
        // if m[1].zv == 0 {println!("1.z {:?}", i)}
        // if m[2].xv == 0 {println!("2.x {:?}", i)}
        // if m[2].yv == 0 {println!("2.y {:?}", i)}
        // if m[2].zv == 0 {println!("2.z {:?}", i)}
        // if m[3].xv == 0 {println!("3.x {:?}", i)}
        // if m[3].yv == 0 {println!("3.y {:?}", i)}
        // if m[3].zv == 0 {println!("3.z {:?}", i)}
        
        // if m[0].xv == 0 && m[0].xp == -4 {println!("0.x {:?}", i)}
        // if m[0].yv == 0 && m[0].yp == -9 {println!("0.y {:?}", i)}
        // if m[0].zv == 0 && m[0].zp == -3 {println!("0.z {:?}", i)}
        // if m[1].xv == 0 && m[1].xp == -13 {println!("1.x {:?}", i)}
        // if m[1].yv == 0 && m[1].yp == -11 {println!("1.y {:?}", i)}
        // if m[1].zv == 0 && m[1].zp == 0 {println!("1.z {:?}", i)}
        // if m[2].xv == 0 && m[2].xp == -17 {println!("2.x {:?}", i)}
        // if m[2].yv == 0 && m[2].yp == -7 {println!("2.y {:?}", i)}
        // if m[2].zv == 0 && m[2].zp == 15 {println!("2.z {:?}", i)}
        // if m[3].xv == 0 && m[3].xp == -16 {println!("3.x {:?}", i)}
        // if m[3].yv == 0 && m[3].yp == 4 {println!("3.y {:?}", i)}
        // if m[3].zv == 0 && m[3].zp == 2 {println!("3.z {:?}", i)}
        
        // if m[0].xv == 0 && m[0].xp == -4 && m[0].yv == 0 && m[0].yp == -9 && m[0].zv == 0 && m[0].zp == -3 {println!("0: {:?}", i)}
        // if m[1].xv == 0 && m[1].xp == -13 && m[1].yv == 0 && m[1].yp == -11 && m[1].zv == 0 && m[1].zp == 0 {println!("1: {:?}", i)}
        // if m[2].xv == 0 && m[2].xp == -17 && m[2].yv == 0 && m[2].yp == -7 && m[2].zv == 0 && m[2].zp == 15 {println!("2: {:?}", i)}
        // if m[3].xv == 0 && m[3].xp == -16 && m[3].yv == 0 && m[3].yp == 4 && m[3].zv == 0 && m[3].zp == 2 {println!("3: {:?}", i)} 

        if m[0].xv == 0 && m[0].xp == -4 && m[1].xv == 0 && m[1].xp == -13 && m[2].xv == 0 && m[2].xp == -17 && m[3].xv == 0 && m[3].xp == -16 {
            println!("x: {:?}", i - lastx);
            lastx = i;
        }
        if m[0].yv == 0 && m[0].yp == -9 && m[1].yv == 0 && m[1].yp == -11 && m[2].yv == 0 && m[2].yp == -7 && m[3].yv == 0 && m[3].yp == 4 {
            println!("y: {:?}", i - lasty);
            lasty = i;
        }
        if m[0].zv == 0 && m[0].zp == -3 && m[1].zv == 0 && m[1].zp == 0 && m[2].zv == 0 && m[2].zp == 15 && m[3].zv == 0 && m[3].zp == 2 {
            println!("z: {:?}", i - lastz);
            lastz = i;
        }
    }

    let mut total_energy = 0;
    for a in 0..4{
        total_energy += (i64::abs(m[a].xp) + i64::abs(m[a].yp) + i64::abs(m[a].zp)) * (i64::abs(m[a].xv) + i64::abs(m[a].yv) + i64::abs(m[a].zv));
    }

    println!("{:?}", m);
    println!("{:?}", total_energy);
}
